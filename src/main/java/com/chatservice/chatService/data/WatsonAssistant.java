package com.chatservice.chatService.data;


import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.cloud.sdk.core.service.exception.NotFoundException;
import com.ibm.cloud.sdk.core.service.exception.RequestTooLargeException;
import com.ibm.cloud.sdk.core.service.exception.ServiceResponseException;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class WatsonAssistant {

    private IamAuthenticator authenticator;
    private Assistant assistance;
    private MessageInput messageInput;
    private MessageOptions messageOptions;
    private MessageResponse messageResponse;
    private CreateSessionOptions sessionOptions;
    private SessionResponse sessionResponse;
    private DeleteSessionOptions deleteSessionOptions;
    private String sessionId;
    private List<RuntimeEntity> entities;
    private String recordedConversation = "";
    private LogService logService = new LogService();


    private String KEY = "OfB1F3zDf_pm7QBxWxjqXWS6NcunW7y2Qze12WxfpaaV";
    private String URL = "https://gateway.watsonplatform.net/assistant/api";
    private String ASSISTANT_ID = "95fe2f6f-61f4-4fb4-9158-633ca2ef4c4a";
    private String VERSION_DATE = "2019-02-28";
    private String MESSAGE_TYPE = "text";



    public void init() {
        authenticator = new IamAuthenticator(KEY);
        assistance = new Assistant(VERSION_DATE, authenticator);
        assistance.setServiceUrl(URL);

    }

    public String createSession() {
        init();
        try {
            sessionOptions = new CreateSessionOptions.Builder(ASSISTANT_ID).build();
            sessionResponse = assistance.createSession(sessionOptions).execute().getResult();
            sessionId = sessionResponse.getSessionId();
            recordedConversation = "";
        } catch (NotFoundException e) {
            // Handle Not Found (404) exception
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();

        } catch (RequestTooLargeException e) {
            // Handle Request Too Large (413) exception
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        } catch (ServiceResponseException e) {
            // Base class for all exceptions caused by error responses from the service
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        }

        return sessionId;
    }

    public String deleteSession() {
        try {
            // Invoke a Watson Assistant method
        } catch (NotFoundException e) {
            // Handle Not Found (404) exception
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        } catch (RequestTooLargeException e) {
            // Handle Request Too Large (413) exception
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        } catch (ServiceResponseException e) {
            // Base class for all exceptions caused by error responses from the service
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        }

        deleteSessionOptions = new DeleteSessionOptions.Builder(ASSISTANT_ID, sessionId).build();

        return assistance.deleteSession(deleteSessionOptions).execute().getStatusMessage();
    }

    public String sendMessage(String message) {


        try {
            messageInput = new MessageInput.Builder()
                    .messageType(MESSAGE_TYPE)
                    .text(message)
                    .build();
            messageOptions  = new MessageOptions.Builder(ASSISTANT_ID, sessionId)
                    .input(messageInput)
                    .build();

            messageResponse = assistance.message(messageOptions).execute().getResult();
            entities = messageResponse.getOutput().getEntities();
            logService.logTransaction("Message sended",String.valueOf(200));
            return messageResponse.getOutput().getGeneric().get(0).text();
        } catch (NotFoundException e) {
            // Handle Not Found (404) exception
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        } catch (RequestTooLargeException e) {
            // Handle Request Too Large (413) exception
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        } catch (ServiceResponseException e) {
            // Base class for all exceptions caused by error responses from the service
            logService.logException(e.getMessage());
            return "Service returned status code " +
                    e.getStatusCode() + ": " + e.getMessage();
        }

    }

    public List<RuntimeEntity> getEntities() {
        return entities;
    }

    public void appendConversation(String response){
        recordedConversation = recordedConversation.concat(response);
    }
    public String getRecordedConversation(){
        return recordedConversation;
    }


}
