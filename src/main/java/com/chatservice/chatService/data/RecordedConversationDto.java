package com.chatservice.chatService.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("Model Recorded Conversation")
public class RecordedConversationDto {
    @ApiModelProperty(value = "Revorded conversation user input")
    String conversation;
}
