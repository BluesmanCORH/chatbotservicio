package com.chatservice.chatService.data;


import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class LogService {


    private String URL_LOG_TRANSACTION = "https://coe-logging.herokuapp.com/logTransaction";
    private String URL_LOG_EXCEPTION = "https://coe-logging.herokuapp.com/logException";
    private String SERVICE = "CHATBOT";
    private RestTemplate template;
    private HttpHeaders headers;

    public  LogService(){

    }
    public void logTransaction(String message,String status){
        template = new RestTemplate();
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap();
        map.add("message", message);
        map.add("status", status);
        map.add("service", SERVICE);



        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        String result = template.postForObject(URL_LOG_TRANSACTION, request, String.class);
        System.out.print("save transaction: "+ result);

    }
    public void logException(String message){
        template = new RestTemplate();
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap();
        map.add("message", message);
        map.add("service", SERVICE);



        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        String result = template.postForObject(URL_LOG_EXCEPTION, request, String.class);
        System.out.print("save transaction: "+ result);
    }



}
