package com.chatservice.chatService.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel("Model Session")
public class SessionDto {
    @ApiModelProperty(value = "Session id")
    String sessionId;
}
