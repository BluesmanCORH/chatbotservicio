package com.chatservice.chatService.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
@ApiModel("Model Message")
public class MessageDto {
    @ApiModelProperty(value = "The message response")
    String message;
    @ApiModelProperty(value = "Call Events endpoint flag")
    boolean callEvents;
}
