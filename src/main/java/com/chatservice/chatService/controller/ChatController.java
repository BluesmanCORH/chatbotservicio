package com.chatservice.chatService.controller;

import com.chatservice.chatService.data.EventDto;
import com.chatservice.chatService.data.MessageDto;
import com.chatservice.chatService.data.RecordedConversationDto;
import com.chatservice.chatService.data.SessionDto;
import com.chatservice.chatService.service.ChatService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/chat")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@Api(value = "Event Chat with Watson", description = "This API is the gateway between Eventbrite and Watson assistant")
public class ChatController {

    @Autowired
    ChatService chatService;


    @GetMapping("/createSession")
    @HystrixCommand(fallbackMethod = "sessionNotAvailable")
    @ApiOperation(value = "Get Session", notes = "Return a session id, necessary to start a conversation")
    public SessionDto createSession(){
        SessionDto sessiondto = new SessionDto();
        try{
            sessiondto.setSessionId(chatService.createSession());
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return  sessiondto;
    }
    public SessionDto sessionNotAvailable(){
        return new SessionDto();
    }


    @GetMapping("/recordedConversation")
    @ApiOperation(value = "Recorded Conversation",notes = "Returns the user input")
    public RecordedConversationDto recordedConversation(){
        RecordedConversationDto result = new RecordedConversationDto();
        try{
            result.setConversation(chatService.recordedConversation());
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return result;
    }


    @GetMapping("/conversation/{message}")
    @ApiOperation(value = "Send a message to Watson", notes = "Return a respose by Watson")
    public MessageDto sendMessage(@PathVariable("message") String message){
        chatService.appendConversation(message+" | ");
        MessageDto response = chatService.sendMessage(message);


        return response;
    }


    @GetMapping("/events")
    @ApiOperation(value = "Eventbrite Events",notes = "Return all the specific events that user needs ")
    public List<EventDto> getEvents(){
        return  chatService.getEvents();
    }


}
