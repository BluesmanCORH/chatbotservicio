package com.chatservice.chatService.service;

import com.chatservice.chatService.data.EventDto;
import com.google.gson.annotations.JsonAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventCaller {

    private String url = "https://evenbriteconsulting.herokuapp.com/events/";

    public ArrayList<EventDto> getEvents(String keyword) {

        ArrayList<EventDto> events = new ArrayList();

        url = url.concat(keyword);
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<String> entity = new HttpEntity<>(new HttpHeaders());
            ResponseEntity<List<EventDto>> result = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<EventDto>>() {
            });

            events = new ArrayList<EventDto>(result.getBody());

        } catch (HttpServerErrorException ex){
            return  events;
        } catch(Exception e){
            return events;
        }
        return events;

    }


}
