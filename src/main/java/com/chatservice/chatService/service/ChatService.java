package com.chatservice.chatService.service;

import com.chatservice.chatService.data.EventDto;
import com.chatservice.chatService.data.MessageDto;
import com.chatservice.chatService.data.WatsonAssistant;
import com.ibm.watson.assistant.v2.model.RuntimeEntity;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ChatService {

    @Autowired
    private WatsonAssistant watsonAssistant;

    @Autowired
    private EventCaller eventCaller;


    private List<EventDto> events;

    public String createSession() {
        return watsonAssistant.createSession();
    }

    public String closeSession() {
        return watsonAssistant.deleteSession();
    }

    public MessageDto sendMessage(String message) {
        String result = watsonAssistant.sendMessage(message);
        List<RuntimeEntity> entities = watsonAssistant.getEntities();
        MessageDto messageResult = new MessageDto();
        JSONObject objectResult = new JSONObject();
        //List<EventDto> events;
        try{
            if (!entities.isEmpty()) {
                events = eventCaller.getEvents(entities.get(0).value());
                if (events.isEmpty()) {
                    result += "\n No events available try with another keyword or there is a problem with Eventbrite :(";
                    messageResult.setCallEvents(false);
                } else {
                    // System.out.println(events);
                    result += "\n Maybe can you be interested in these events:  \n";
                    messageResult.setCallEvents(true);
                }
            }

           messageResult.setMessage(result);
        }catch (Exception ex){

        }


        return messageResult;
    }


    public void appendConversation(String conversation){
        watsonAssistant.appendConversation(conversation);
    }
    public String recordedConversation(){
        return  watsonAssistant.getRecordedConversation();
    }
    public  List<EventDto> getEvents(){
        return events;
    }
}
